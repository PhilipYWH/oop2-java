package day04session;

import java.util.Arrays;

public class App {

	public static void main(String[] args) {
    
		Integer [] is = new Integer[3];
		
		is[0] = 12;
		is[1]  = Integer.parseInt("13");
		is[2]  = null;

        System.out.println(Arrays.toString(is));
		
        
        String s= "all small case";
        s.concat("add something new");
        System.out.println(s);
        
        StringBuilder builder = new StringBuilder();
        System.out.println(builder);
        builder.append("this is the 1st string. ");
        builder.append("this is the 2nd string. ");
        System.out.println(builder);
        
        int si = builder.indexOf("this is the 2nd");
        builder.insert(si, "THIS IS THE 3RD STRING WHICH INSERT BETWEEN 1ST AND SND STRING. ");
        System.out.println(builder);
        
        builder.replace(0, 10, "THI ");
        System.out.println(builder);
        
        
	}

}
