package day3datatype;

import java.util.Arrays;

public class StringType {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s1 = "hello";
		String s2 = "hello";
		
		
		System.out.println("s1 == s2 : " + (s1==s2));
		System.out.println("s1.equals(s2) : " + (s1.equals(s2)));
		
		
		String s3 = "hello";
		String s4 = "HEllO".toLowerCase();
		
		System.out.println("s3 == s4 : " + (s3==s4));
		System.out.println("s3.equals(s4) : " + (s3.equals(s4)));
		
		
		System.out.println("hello.startWith(\"h\"):"+s1.startsWith("h"));
		System.out.println("hello.endsWith(\"o\"):"+s1.endsWith("o"));
		System.out.println("hello.indexOf(\"e\"):"+s1.indexOf("e"));
		System.out.println("hello.lastIndexOf(\"l\"):"+s1.lastIndexOf("l"));
		System.out.println("hello.contains(\"e\"):"+s1.contains("e"));
		
		String s5 = "h!e l;l?o";
		System.out.println("s5= " + s5);
//		s5.replaceAll("[\\s\\!\\;\\?]+", ",");
//		s5.replaceAll("[\\,\\;\\s]+", ",");
		System.out.println("After s5.replaceAll, s5= " + s5);
		
		String[] strArr = {"A","B","C"};
		System.out.println("String Array = "+Arrays.toString(strArr));
		String joinCharArr = String.join("***",strArr);
		System.out.println("Join charArr =" + joinCharArr);
		
		
		//type conversion
		System.out.println("Integer.parseInt(\"254\") : " + Integer.parseInt("254") );
		System.out.println("Integer.parseInt(\"ff, 16\") : " + Integer.parseInt("ff",16) );
		System.out.println("Boolean.parseBoolean(\"FALSE\") : " + Boolean.parseBoolean("FALSE") );
	}

}
